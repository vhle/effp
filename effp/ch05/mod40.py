"""Initialize parent classes with super"""


class MyBaseClass:
    def __init__(self, value):
        self.value = value


class TimesSeven(MyBaseClass):
    def __init__(self, value):
        super().__init__(value)
        self.value *= 7


class PlusNine(MyBaseClass):
    def __init__(self, value):
        super().__init__(value)
        self.value += 9


class Goodway(TimesSeven, PlusNine):
    def __init__(self, value):
        super().__init__(value)


foo = Goodway(5)
assert foo.value == (5 + 9) * 7
print(Goodway.mro())
