""": Accept Functions Instead of Classes for Simple Interfaces"""


from collections import defaultdict


# Demo using hooks
def log_missing():
    print('Key added')
    return 0


current = {'green': 12, 'blue': 3}
increments = [
    ('red', 5),
    ('blue', 17),
    ('orange', 9),
]

result = defaultdict(log_missing, current)
print('Before:', dict(result))
for key, amount in increments:
    result[key] += amount

print('After:', dict(result))


# Demo: using __call__ function instead of closure
class CountMissing:
    def __init__(self):
        self.added = 0

    def __call__(self):
        self.added += 1
        return 0


counter = CountMissing()
assert counter.added == 0
assert callable(counter)
result = defaultdict(counter, current)
for key, amount in increments:
    result[key] += amount
assert counter.added == 2
