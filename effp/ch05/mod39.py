"""Using @classmethod Polymophism to construct objects generically"""


import abc
import pathlib
import random
import tempfile
from threading import Thread


class GenericInputData(abc.ABC):
    @abc.abstractmethod
    def read(self):
        """Read data. To be implemented by sub-classes."""

    @classmethod
    @abc.abstractmethod
    def generate_inputs(cls, config):
        """Construct objects here."""


class Worker(abc.ABC):
    def __init__(self, input_data):
        self.input_data = input_data
        self.result = None

    @abc.abstractmethod
    def map(self):
        """Map operation."""

    @abc.abstractmethod
    def reduce(self, other):
        """Reduce operation"""

    @classmethod
    def create_workers(cls, input_class: GenericInputData, config):
        workers = []
        for input_data in input_class.generate_inputs(config):
            workers.append(cls(input_data))
        return workers


class PathInputData(GenericInputData):
    def __init__(self, path):
        super().__init__()
        self.path = path

    @classmethod
    def generate_inputs(cls, config):
        data_dir = pathlib.Path(config['data_dir'])
        for f in data_dir.iterdir():
            yield cls(f)

    def read(self):
        with open(self.path) as f:
            return f.read()


class LineCountWorker(Worker):
    def map(self):
        data = self.input_data.read()
        self.result = data.count('\n')

    def reduce(self, other):
        self.result += other.result


def execute(workers):
    threads = [Thread(target=w.map) for w in workers]
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    first, *rest = workers
    for worker in rest:
        first.reduce(worker)
    return first.result


def mapreduce(worker_class: type(Worker), input_class, config):
    workers = worker_class.create_workers(input_class, config)
    return execute(workers)


def write_test_files(tmpdir):
    tmpdir = pathlib.Path(tmpdir)
    tmpdir.mkdir(parents=True, exist_ok=True)
    lines_written = 0
    for i in range(101):
        with open(tmpdir / str(i), 'w') as f:
            lines = random.randint(0, 100)
            f.write('\n' * lines)
            lines_written += lines
    return lines_written


def test_mapreduce():
    tmpdir = tempfile.mkdtemp()
    lines_written = write_test_files(tmpdir)
    config = {'data_dir': tmpdir}
    result = mapreduce(LineCountWorker, PathInputData, config)
    assert lines_written == result
