"""Be careful when relying on dict insertion order.

Since Python 3.6 (and officially in Python 3.7), Python preserves the order of insertion in dictionary. Therefore,
the behavior of dict and OrderedDict is the same.

In newer Python (>= 3.6), we can assume the order of
- keyword arguments in functions
- instance fields in objects

However, still be careful because the order may not be preserved in user-defined dictionary types.
"""

baby_names = {
    'cat': 'kitten',
    'dog': 'puppy'
}

print(baby_names)
