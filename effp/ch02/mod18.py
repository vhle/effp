""" Know How to Construct Key-Dependent Default Values"""


def open_picture(profile_path):
    try:
        return open(profile_path, 'a+b')
    except OSError:
        print(f'Failed to open path {profile_path}')
        raise


class Pictures(dict):
    """Collection of pictures."""

    def __missing__(self, key):
        value = open_picture(key)
        self[key] = value
        return value
