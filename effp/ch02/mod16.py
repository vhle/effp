"""Prefer `get` over `in` and KeywordError to handle missing dictionary keys"""


counters = {
    'pumpernickel': 2,
    'sourdough': 1
}


key = 'wheat'

# Awkward
if key in counters:
    count = counters[key]
else:
    count = 0

# Good
count = counters.get(key, 0)
counters[key] = count + 1


# Another example
votes = {
    'baguette': ['Bob', 'Alice'],
    'ciabatta': ['Coco', 'Deb'],
}
key = 'brioche'
who = 'Elmer'


# Good
names = votes.get(key)
if names is None:
    votes[key] = names = []
names.append(who)

# Good, Python >- 3.8
if (name := votes.get(key)) is None:
    votes[key] = name = []
names.append(who)


# Another way: setdefault. This is shorter, but less readable.
names = votes.setdefault(key, [])
names.append(who)
