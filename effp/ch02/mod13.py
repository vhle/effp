"""Prefer catch-all unpacking over slicing"""

car_ages = [0, 9, 4, 8, 7, 20, 19, 1, 6, 15]
car_ages_descending = sorted(car_ages, reverse=True)

# catch-all unpacking: star expression
oldest, second_oldest, *_ = car_ages_descending

print(f'Age of oldest car: {oldest}')
print(f'Age of second oldest car: {second_oldest}')


# The star expression can be any where in the statement
oldest, *_, newest = car_ages_descending
print(f'Age of newest car: {newest}')
