"""Sort by complex criteria using the key parameter"""


class Tool:
    def __init__(self, name, weight):
        self.name = name
        self.weight = weight

    def __repr__(self):
        return f'Tool({self.name!r}, {self.weight})'


tools = [
    Tool('level', 3.5),
    Tool('hammer', 1.25),
    Tool('screwdriver', 0.5),
    Tool('chisel', 0.25),
]

tools.sort(key=lambda t: t.name)
print('Tools, sorted by name:', tools)

tools.sort(key=lambda t: t.weight, reverse=True)
print('Tools, heaviest ones first:', tools)


places = ['home', 'work', 'New York', 'Paris']
print('Sorted (case sensitive):', sorted(places))

sorted_places = sorted(places, key=lambda x: x.lower())
print('Sorted (case insensitive):', sorted_places)
