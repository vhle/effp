"""Know to to slice sequences"""


a = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']


# Basic form of slicing
assert a[3:5] == ['d', 'e']
assert a[1:7] == ['b', 'c', 'd', 'e', 'f', 'g']


# Slicing is forgiving: if either the beginning and the end is
# out of range, slicing returns the available elements without
# raising and error.
assert a[3:10000] == a[3:]
assert a[-200:] == a


# Modifying a list with slicing
b = a
a[:] = [100, 101, 102]
assert a is b
assert a == [100, 101, 102]
