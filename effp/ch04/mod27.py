"""Use comprehensions instead of map and filter"""


a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
squares = [x * x for x in a]
even_squares = [s for s in squares if s % 2 == 0]
print(a)
print(squares)
print(even_squares)
