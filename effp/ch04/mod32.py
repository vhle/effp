"""Consider generator expressions for large list comprehensions"""

with open('my_numbers.txt') as f:
    values = [float(x) for x in f]
print(values)

f = open('my_numbers.txt')
it = (float(x) for x in f)
roots = ((x, x ** 0.5) for x in it)
while True:
    try:
        print(next(roots))
    except StopIteration:
        break
f.close()
