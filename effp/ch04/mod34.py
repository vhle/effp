"""Avoid injecting data into generators with send"""

import math


def wave(amplitude_it, steps):
    step_size = 2 * math.pi / steps
    for step in range(steps):
        radians = step * step_size
        fraction = math.sin(radians)
        amplitude = next(amplitude_it)
        output = amplitude * fraction
        yield output


def transmit(output):
    if output is None:
        print('Output is None')
    else:
        print(f'Output: {output:>5.1f}')


def complex_wave(amplitude_it):
    yield from wave(amplitude_it, 3)
    yield from wave(amplitude_it, 4)
    yield from wave(amplitude_it, 5)


def run():
    amplitudes = [7, 7, 7, 2, 2, 2, 2, 10, 10, 10, 10, 10]
    it = complex_wave(iter(amplitudes))
    for output in it:
        transmit(output)


run()
