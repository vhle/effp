"""Be defensive when iterating over arguments"""


import pytest


def normalize(numbers):
    # This won't work if numbers is an iterator and not a list.
    total = sum(numbers)
    result = []
    for value in numbers:
        percent = 100 * value / total
        result.append(percent)
    return result


def read_visits(data_path):
    with open(data_path) as f:
        for line in f:
            yield int(line)


# This does not work: percentages == []
it = read_visits('my_numbers.txt')
percentages = normalize(it)
print(percentages)


class ReadNumbers:
    def __init__(self, data_path):
        self.data_path = data_path

    def __iter__(self):
        with open(self.data_path) as f:
            for line in f:
                yield int(line)


reader = ReadNumbers('my_numbers.txt')
percentages = normalize(reader)
print(percentages)


def normalize_defensive(numbers):
    if iter(numbers) is numbers:
        raise TypeError('Must supply a container.')
    total = sum(numbers)
    return [100 * value / total for value in numbers]


with pytest.raises(TypeError):
    it = read_visits('my_numbers.txt')
    percentages = normalize_defensive(it)
    print(percentages)

percentages = normalize_defensive(reader)
print(percentages)
