"""Compose multiple generators with `yield from`"""


# Example 1: compose with yield from
import timeit


def move(period, speed):
    for _ in range(period):
        yield speed


def pause(period):
    for _ in range(period):
        yield 0


def animate():
    yield from move(4, 5.0)
    yield from pause(3)
    yield from move(2, 3.0)


def render(delta):
    print(f'Delta: {delta:.1f}')


def run(func):
    for delta in func():
        render(delta)


run(animate)


# Example 2: speed up with `yield from`


def child():
    for i in range(1_000_000):
        yield i


def slow():
    for i in child():
        yield i


def fast():
    yield from child()


baseline = timeit.timeit(
    stmt='for _ in slow(): pass',
    globals=globals(),
    number=50
)

comparison = timeit.timeit(
    stmt='for _ in fast(): pass',
    globals=globals(),
    number=50
)

print(f'Manual nesting: {baseline:.2f}s')
print(f'Composed nesting: {comparison:.2f}')
print(f'Speedup: {baseline / comparison:.2f}x')
