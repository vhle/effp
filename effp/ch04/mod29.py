"""Avoid Repeated Work in Comprehensions by Using Assignment Expressions"""

stock = {
    'nails': 125,
    'screws': 35,
    'wingnuts': 8,
    'washers': 24,
}


def get_batches(count, size):
    return count // size


result = {}
order = ['screws', 'wingnuts', 'clips']

# OK: using loop
for name in order:
    count = stock.get(name, 0)
    batches = get_batches(count, 8)
    if batches:
        result[name] = batches
print(result)

# Bad: stock.get is called twice for each name
found = {
    name: get_batches(stock.get(name, 0), 8)
    for name in order
    if get_batches(stock.get(name, 0), 8)
}
assert found == result

# Better
found = {
    name: batches for name in order
    if (batches := get_batches(stock.get(name, 0), 8))
}
assert found == result
