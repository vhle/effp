"""Avoid More Than Two Control Subexpressions in Comprehensions"""


matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

# OK: 2 control sub-expressions
flat = [x for row in matrix for x in row]
print(flat)

# OK
squares = [[x ** 2 for x in row]
           for row in matrix]
print(squares)


# Hard to read
my_lists = [
    [[1, 2, 3], [4, 5, 6]],
    [[7, 8, 9], [10, 11, 12]]
]
flat = [x for sublist1 in my_lists
        for sublist2 in sublist1
        for x in sublist2]
print(flat)

# More readable
flat = []
for sublist1 in my_lists:
    for sublist2 in sublist1:
        flat.extend(sublist2)
print(flat)

# OK
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
b = [x for x in a if x > 4 if x % 2 == 0]
c = [x for x in a if x > 4 and x % 2 == 0]
print(a)
print(b)
print(c)

# Hard to read
matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
filtered = [[x for x in row if x % 3 == 0]
            for row in matrix if sum(row) >= 10]
print(filtered)

# Better
filtered = []
for row in matrix:
    if sum(row) > 10:
        filtered_row = [x for x in row if x % 3 == 0]
        filtered.append(filtered_row)
print(filtered)
