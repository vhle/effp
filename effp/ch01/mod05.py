"""
Write helper functions instead of complex expressions
"""

from urllib.parse import parse_qs


def get_first_int(values, key, default=0):
    found = values.get(key, [''])
    if found[0]:
        return int(found[0])
    return default


my_values = parse_qs('red=5&green=&blue=0', keep_blank_values=True)

red = get_first_int(my_values, 'red', default=0)
green = get_first_int(my_values, 'green', default=0)
blue = get_first_int(my_values, 'blue', default=0)

print(f'red: {red}, green: {green}, blue: {blue} )')
