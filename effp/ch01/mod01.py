"""
Knowing which version of Python you are using

From command-line:
$ python --version
"""


import sys
print(sys.version)
