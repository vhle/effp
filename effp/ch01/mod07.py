"""Prefer enumerate over range"""

flavors = ('vanilla', 'chocolate', 'pecan', 'strawberry')


# Awkward example
for i in range(len(flavors)):
    print(f'#{i + 1}: {flavors[i]}')

# Good example
for index, flavor in enumerate(flavors, 1):
    print(f'#{index}: {flavor}')
