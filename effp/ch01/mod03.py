"""
Know the differences between `bytes` and `str`

* bytes contain unsigned 8-bit values
* str contains Unicode codepoints representing textual characters.

"""


def to_str(bytes_or_str):
    """Convert a value which can be either a bytes or a str to string"""
    if isinstance(bytes_or_str, bytes):
        value = bytes_or_str.decode('utf-8')
    else:
        value = bytes_or_str

    return value


def to_bytes(bytes_or_str):
    """Convert a value which can be either a bytes or a str to a bytes"""
    if isinstance(bytes_or_str, bytes):
        value = bytes_or_str
    else:
        value = bytes_or_str.encode('utf-8')
    return value
