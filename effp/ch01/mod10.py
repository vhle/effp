""" Prevent Repetition with Assignment Expressions

(Assignment Expression/The walrus operator is new in Python 3.8)
"""

fresh_fruit = {
    'apple': 10,
    'banana': 8,
    'lemon': 5,
}


def make_lemonade(count):
    print(f'Making {count} lemonade drink(s) ...')
    fresh_fruit['lemon'] -= count
    print('Done')


def make_cider(count):
    print('Making cider...')
    fresh_fruit['apple'] -= count
    print('Done')


def out_of_stock():
    print('Out of stock')


if count := fresh_fruit.get('lemon', 0):
    make_lemonade(count)
else:
    out_of_stock()


if (count := fresh_fruit.get('apple', 0)) >= 4:
    make_cider(count)
else:
    out_of_stock()
