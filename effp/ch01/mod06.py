"""
Prefer multiple assignment unpacking over indexing
"""


# First example of unpacking
def bubble_sort(a):
    """Perform in-place bubble sort on array a."""
    for _ in range(len(a)):
        for i in range(1, len(a)):
            if a[i - 1] > a[i]:
                a[i - 1], a[i] = a[i], a[i - 1]


names = ['pretzels', 'carrots', 'arugula', 'bacon']
bubble_sort(names)
print(names)


snacks = [('bacon', 350), ('donut', 240), ('muffin', 190)]


# Second example of unpacking
snacks = [('bacon', 350), ('donut', 240), ('muffin', 190)]
for rank, (name, calories) in enumerate(snacks, 1):
    print(f'#{rank}: {name} has {calories} calories')
