"""
Never unpack more than 3 variables when functions return multiple values.
"""

from typing import NamedTuple


class ListStat(NamedTuple):
    minimum: float = 0
    maximum: float = 0
    count: int = 0
    average: float = 0
    median: float = 0


def get_stats(numbers):
    minimum = min(numbers)
    maximum = max(numbers)
    count = len(numbers)
    average = sum(numbers) / count

    sorted_numbers = sorted(numbers)
    middle = count // 2
    if count % 2 == 0:
        lower = sorted_numbers[middle - 1]
        upper = sorted_numbers[middle]
        median = (lower + upper) / 2
    else:
        median = sorted_numbers[middle]

    # Bad
    # return minimum, maximum, count, average, median

    # Good
    return ListStat(minimum=minimum, maximum=maximum,
                    count=count, average=average, median=median)


lengths = [63, 73, 72, 60, 67, 66, 71, 61, 72, 70]
minimum, maximum = get_stats(lengths)
print(f'Minimum: {minimum}, maximum: {maximum}')
