"""Prefer raising exception rather than returning None"""


def careful_divide_akward(a, b):
    try:
        return a / b
    except ZeroDivisionError:
        return None


a, b = 0, 10
if not careful_divide_akward(a, b):
    print('Invalid arguments.')


# Good version below.


def careful_divide(a, b):
    """Divide a by b.

    Raises:
        ValueError: When the inputs cannot be divided.
    """
    try:
        return a / b
    except ZeroDivisionError:
        return ValueError('Invalid inputs')
